/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *  _     _     _           ___          _                      _       
 * | |   (_)___| |_ __ _   / _ \ _ __ __| | ___ _ __   __ _  __| | __ _ 
 * | |   | / __| __/ _` | | | | | '__/ _` |/ _ \ '_ \ / _` |/ _` |/ _` |
 * | |___| \__ \ || (_| | | |_| | | | (_| |  __/ | | | (_| | (_| | (_| |
 * |_____|_|___/\__\__,_|  \___/|_|  \__,_|\___|_| |_|\__,_|\__,_|\__,_|
 *                                                                       
 */

#include "linked_list.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define INSERE "INSERE"
#define REMOVE "REMOVE"
#define IMPRIMIR "IMPRIMIR"


int main()
{
	Person *list = create_list();

	while (!feof(stdin)) {
		char command[9];
		bool stop = scanf(" %s", command) == EOF;
		getchar();

		if (stop)
			break;

		if (strcmp(INSERE, command) == 0) {
			Person *new_person = register_person();

			int err = insert(list, new_person);

			if (err == -1) {
				printf("INVALIDO\n");
				destroy(new_person);
			}

		} else if (strcmp(REMOVE, command) == 0) {
			int16_t code;
			scanf(" %hd", &code);
			getchar();

			int err = take(list, code);

			if (err == -1)
				printf("INVALIDO\n");

		} else if (strcmp(IMPRIMIR, command) == 0) {
			print_list(list);

		} else {
			printf("INVALIDO\n");
			print_list(list);
		}
	}

	destroy(list);

	return 0;
}
