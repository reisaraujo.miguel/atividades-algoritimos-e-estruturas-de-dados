#include "linked_list.h"
#include "string_plus.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct person {
	char *name;
	int16_t code;
	Person *next;
};


Person *create_list(void)
{
	Person *head = malloc(sizeof(Person));
	head->code = -1;
	head->next = NULL;

	return head;
}


int insert(Person *list, Person *person)
{
	if (list->next == NULL && list->code == -1) { // lista vazia
		list->next = person;

	} else {
		if (list->next != NULL && list->next->code == person->code) {
			return -1;

		} else if (list->next == NULL || list->next->code > person->code) {
			person->next = list->next;
			list->next = person;

		} else {
			return insert(list->next, person);
		}
	}

	return 0;
}


int take(Person *list, int16_t code)
{
	if (list->next == NULL) {
		return -1; // não achou a pessoa cadastrada
	}

	if (list->next->code == code) {
		Person *to_remove = list->next;

		list->next = list->next->next;

		free(to_remove->name);
		free(to_remove);

	} else {
		return take(list->next, code);
	}

	return 0;
}


void print_list(Person *list)
{
	if (list->next == NULL && list->code == -1) {
		printf("VAZIA\n");
		return;

	} else if (list->next == NULL) {
		putchar('\n');
		return;
	}

	printf("%hd, %s; ", get_code(list->next), get_name(list->next));

	print_list(list->next);
}


char *get_name(Person *person)
{
	return person->name;
}


int16_t get_code(Person *person)
{
	return person->code;
}


Person *register_person()
{
	Person *new_person = malloc(sizeof(Person));

	scanf(" %hd", &new_person->code);
	getchar();

	new_person->name = read_line(stdin);

	new_person->next = NULL;

	return new_person;
}


void destroy(Person *list)
{
	Person *next = list->next;

	if (list->code != -1)
		free(list->name);

	free(list);

	if (next != NULL)
		destroy(next);
}
