#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <stdint.h>

typedef struct person Person;

Person *create_list(void);

int insert(Person *list, Person *person);

int take(Person *list, int16_t code);

void print_list(Person *list);

Person *register_person(void);

char *get_name(Person *person);

int16_t get_code(Person *person);

void destroy(Person *list);

#endif /* LINKED_LIST_H */
