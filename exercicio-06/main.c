/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202 / SCC0201
 *
 * Título Trabalho:
 *  ____       _                              _        _     _     _        
 * |  _ \ ___ | |_ __ _  ___ __ _  ___     __| | ___  | |   (_)___| |_ __ _ 
 * | |_) / _ \| __/ _` |/ __/ _` |/ _ \   / _` |/ _ \ | |   | / __| __/ _` |
 * |  _ < (_) | || (_| | (_| (_| | (_) | | (_| |  __/ | |___| \__ \ || (_| |
 * |_| \_\___/ \__\__,_|\___\__,_|\___/   \__,_|\___| |_____|_|___/\__\__,_|
 *                                                                        
 */


#include "linked_list.h"
#include <stdio.h>

void print_list(LinkedList *list, int size);


int main()
{
	int loop_num;
	scanf(" %d", &loop_num);

	for (int i = 0; i < loop_num; i++) {
		int list_size, tot_rotations;
		scanf(" %d %d", &list_size, &tot_rotations);

		LinkedList *list = create();
		for (int i = 0; i < list_size; i++) {
			int value;
			scanf(" %d", &value);

			insert(list, value);
		}

		for (int i = 0; i < (tot_rotations % list_size); i++) {
			rotate(list);
		}

		print_list(list, list_size);

		destroy(list);
	}

	return 0;
}


void print_list(LinkedList *list, int size)
{
	for (int i = 0; i < size; i++)
		printf("%d ", get(list));

	putchar('\n');
}
