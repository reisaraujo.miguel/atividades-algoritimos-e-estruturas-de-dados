#ifndef CHECK_PAIRS_H
#define CHECK_PAIRS_H

#include <stdbool.h>

typedef struct stack Stack;

bool is_balanced(char *string);

Stack *create(int size);

void push(Stack *my_stack, char value);

char pop(Stack *my_stack);

char top(Stack *my_stack);
// accessa o último valor adicionado na stack sem removê-lo.

bool is_empty(Stack *my_stack);

void empty(Stack *my_stack);

#endif /* CHECK_PAIRS_H */
