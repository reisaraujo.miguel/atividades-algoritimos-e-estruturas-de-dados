#ifndef SKIP_LIST_H
#define SKIP_LIST_H

#include <stdbool.h>

typedef struct skip_list SkipList;
typedef struct node Node;
typedef struct elem Elem;

SkipList *create_skip_list(void);

bool sk_insert(SkipList *skip_list, char *word, char *definition);

Node *sk_search(SkipList *skip_list, char *word);

bool sk_change(SkipList *skip_list, char *word, char *definition);

bool sk_remove(SkipList *skip_list, char *word);

bool sk_print(SkipList *skip_list, char *initial);

void node_print(Node *node);

bool destroy(SkipList *skip_list);

#endif /* SKIP_LIST_H */
