/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *  ____  _    _         _     _     _       
 * / ___|| | _(_)_ __   | |   (_)___| |_ ___ 
 * \___ \| |/ / | '_ \  | |   | / __| __/ __|
 *  ___) |   <| | |_) | | |___| \__ \ |_\__	\
 * |____/|_|\_\_| .__/  |_____|_|___/\__|___/
 *              |_|                          
 */

#include "skip_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "string_plus.h"
#include <time.h>

int main()
{
	srand(time(NULL));
	
	SkipList *list = create_skip_list();

	while (!feof(stdin)) {
		char command[11];
		bool stop = scanf(" %s", command) == EOF;
		getchar();

		if (stop) {
			break;
		}

		if (strcmp(command, "insercao") == 0) {
			char *word = read_word(stdin);
			char *definition = read_line(stdin);

			bool success = sk_insert(list, word, definition);

			if (!success) {
				free(word);
				free(definition);
				printf("OPERACAO INVALIDA\n");
			}

		} else if (strcmp(command, "alteracao") == 0) {
			char *word = read_word(stdin);
			char *definition = read_line(stdin);

			bool success = sk_change(list, word, definition);

			free(word);

			if (!success) {
				free(definition);
				printf("OPERACAO INVALIDA\n");
			}

		} else if (strcmp(command, "remocao") == 0) {
			char *word = read_word(stdin);

			bool success = sk_remove(list, word);

			free(word);

			if (!success)
				printf("OPERACAO INVALIDA\n");

		} else if (strcmp(command, "busca") == 0) {
			char *word = read_word(stdin);

			Node *node = sk_search(list, word);

			if (node == NULL) {
				printf("OPERACAO INVALIDA\n");
			} else {
				node_print(node);
			}

			free(word);

		} else if (strcmp(command, "impressao") == 0) {
			char initial[2];
			scanf(" %c", &initial[0]);
			initial[1] = '\0';

			char aux = getchar();
			if (aux == '\r')
				getchar();

			bool success = sk_print(list, initial);

			if (!success) {
				printf("NAO HA PALAVRAS INICIADAS POR %s\n", initial);
			}
		}
	}

	destroy(list);

	return 0;
}
