#include "skip_list.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

struct skip_list {
	int level;
	int max_level;
	Node *up_left;
};

struct node {
	Elem *value;
	int level;
	Node *next;
	Node *down;
};

struct elem {
	char *word;
	char *definition;
};


SkipList *create_skip_list()
{
	SkipList *new_skip_list = malloc(sizeof(SkipList));

	new_skip_list->max_level = 100;
	new_skip_list->level = 1;

	Node *new_header = malloc(sizeof(Node));

	new_header->value = NULL;
	new_header->level = 0;
	new_header->next = NULL;
	new_header->down = NULL;

	new_skip_list->up_left = new_header;

	return new_skip_list;
}


bool sk_insert(SkipList *skip_list, char *word, char *definition)
{
	if (sk_search(skip_list, word) != NULL) {
		return false;
	}

	Elem *entry = malloc(sizeof(Elem));

	entry->word = word;
	entry->definition = definition;

	Node *sentinel = skip_list->up_left;

	Node *update[skip_list->max_level];

	while (sentinel->level != 0) {
		while (sentinel->next != NULL &&
			   strcmp(sentinel->next->value->word, entry->word) < 0) {
			sentinel = sentinel->next;
		}

		update[sentinel->level] = sentinel;
		sentinel = sentinel->down;
	}

	while (sentinel->next != NULL &&
		   strcmp(sentinel->next->value->word, entry->word) < 0) {
		sentinel = sentinel->next;
	}

	update[sentinel->level] = sentinel;

	int level = 0;
	int node_level = 1;

	while (rand() % 2) {
		node_level++;
	}

	while (level < skip_list->level && level <= node_level) {
		Node *new_node = malloc(sizeof(Node));

		new_node->value = entry;
		new_node->level = level;
		new_node->next = NULL;
		new_node->down = NULL;

		if (level == 0) {
			new_node->next = update[level]->next;
			new_node->down = NULL;
		} else {
			new_node->next = update[level]->next;
			new_node->down = update[level - 1]->next;
		}

		update[level]->next = new_node;
		level++;
	}

	// criando novos niveis
	if (skip_list->level <= node_level) {
		Node *new_header = malloc(sizeof(Node));

		new_header->value = NULL;
		new_header->level = skip_list->level;
		new_header->down = skip_list->up_left;

		skip_list->up_left = new_header;

		Node *new_elem = malloc(sizeof(Node));

		new_elem->value = entry;
		new_elem->level = skip_list->level;
		new_elem->next = NULL;
		new_elem->down = update[skip_list->level - 1]->next;

		new_header->next = new_elem;

		update[skip_list->level] = new_header;

		skip_list->level++;
	}

	//printf("inserido!\n");

	return true;
}


Node *sk_search(SkipList *skip_list, char *word)
{
	Node *node = skip_list->up_left;

	while (node->level != 0) {
		while (node->next != NULL &&
			   strcmp(node->next->value->word, word) < 0) {
			node = node->next;
		}

		if (node->next != NULL && strcmp(node->next->value->word, word) == 0)
			return node->next;

		node = node->down;
	}

	while (node->next != NULL && strcmp(node->next->value->word, word) <= 0) {
		node = node->next;
	}

	if (node->value == NULL || strcmp(node->value->word, word) != 0)
		return NULL;

	else
		return node;
}


bool sk_change(SkipList *skip_list, char *word, char *definition)
{
	Node *node = sk_search(skip_list, word);

	if (node != NULL) {
		free(node->value->definition);
		node->value->definition = definition;
	} else {
		return false;
	}

	//printf("alterado!\n");

	return true;
}


bool sk_remove(SkipList *skip_list, char *word)
{
	if (sk_search(skip_list, word) == NULL) {
		return false;
	}

	Node *sentinel = skip_list->up_left;

	Node *update[skip_list->level];

	while (sentinel->level != 0) {
		while (sentinel->next != NULL &&
			   strcmp(sentinel->next->value->word, word) < 0) {
			sentinel = sentinel->next;
		}

		update[sentinel->level] = sentinel;
		sentinel = sentinel->down;
	}

	while (sentinel->next != NULL &&
		   strcmp(sentinel->next->value->word, word) < 0) {
		sentinel = sentinel->next;
	}

	update[sentinel->level] = sentinel;

	int level = 0;

	Elem *elem = update[level]->next->value;
	free(elem->word);
	free(elem->definition);
	free(elem);

	while (level < skip_list->level && update[level]->next != NULL) {
		Node *to_remove = update[level]->next;

		update[level]->next = to_remove->next;

		free(to_remove);

		level++;
	}

	while (skip_list->up_left->next == NULL) {
		//removendo nivel
		Node *to_remove = skip_list->up_left;
		skip_list->up_left = skip_list->up_left->down;
		free(to_remove);
		skip_list->level--;
	}

	//printf("removido!\n");

	return true;
}


bool sk_print(SkipList *skip_list, char *initial)
{
	Node *node = skip_list->up_left;

	while (node->level != 0) {
		while (node->next != NULL &&
			   strcmp(node->next->value->word, initial) <= 0) {
			node = node->next;
		}

		node = node->down;
	}

	while (node->next != NULL &&
		   strcmp(node->next->value->word, initial) <= 0) {
		node = node->next;
	}

	if (node->next == NULL || strncmp(node->next->value->word, initial, 1) != 0)
		return false;

	while (node->next != NULL &&
		   strncmp(node->next->value->word, initial, 1) == 0) {
		printf("%s %s\n", node->next->value->word,
			   node->next->value->definition);

		node = node->next;
	}

	return true;
}


bool destroy(SkipList *skip_list)
{
	Node *node = skip_list->up_left;

	while (node->level != 0) {
		while (node->next != NULL) {
			Node *to_remove = node->next;
			node->next = to_remove->next;

			free(to_remove);
		}

		skip_list->up_left = node->down;

		free(node);

		node = skip_list->up_left;
	}

	while (node->next != NULL) {
		Node *to_remove = node->next;
		node->next = to_remove->next;

		free(to_remove->value->word);
		free(to_remove->value->definition);
		free(to_remove->value);
		free(to_remove);
	}

	free(node);

	free(skip_list);

	return true;
}


void node_print(Node *node)
{
	printf("%s %s\n", node->value->word, node->value->definition);
}
