/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *  ____        _         _   _ _         _                       _      
 * / ___| _   _| |__  ___| |_(_) |_ _   _(_) ___ __ _  ___     __| | ___ 
 * \___ \| | | | '_ \/ __| __| | __| | | | |/ __/ _` |/ _ \   / _` |/ _	\
 *  ___) | |_| | |_) \__ \ |_| | |_| |_| | | (_| (_| | (_) | | (_| |  __/
 * |____/ \__,_|_.__/|___/\__|_|\__|\__,_|_|\___\__,_|\___/   \__,_|\___|
 *                                                                       
 *  ____  _        _                 
 * / ___|| |_ _ __(_)_ __   __ _ ___ 
 * \___ \| __| '__| | '_ \ / _` / __|
 *  ___) | |_| |  | | | | | (_| \__	\
 * |____/ \__|_|  |_|_| |_|\__, |___/
 *                         |___/     
 */

#include <stdlib.h>
#include <stdio.h>
#include "string_plus.h"
#include <string.h>

int main()
{
	while (!feof(stdin)) {
		char *text = read_line(stdin);

		char *typo = read_line(stdin);

		char *fix = read_line(stdin);

		replace(text, typo, fix);

		printf("%s\n", text);

		free(text);
		free(typo);
		free(fix);
	}

	return 0;
}
