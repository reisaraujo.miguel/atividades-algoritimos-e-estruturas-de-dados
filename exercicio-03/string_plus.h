#ifndef STRING_PLUS_H
#define STRING_PLUS_H

#include <stdio.h>

char *read_line(FILE *stream);
/*
 * Lê uma linha inteira (incluindo espaços) até \n ou EOF
 */

void replace(char *text, char *typo, char *fix);
/*
 * substitui todas as ocorrências de "typo" por "fix" dentro de "text".
 */

#endif /* STRING_PLUS_H */
