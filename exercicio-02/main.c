/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *   ____      _                  _      
 *  / ___|_ __(_)_   _____     __| | ___ 
 * | |   | '__| \ \ / / _ \   / _` |/ _	\
 * | |___| |  | |\ V / (_) | | (_| |  __/
 *  \____|_|  |_| \_/ \___/   \__,_|\___|
 *                                       
 *  _____               _            _                       
 * | ____|_ __ __ _ ___| |_ ___  ___| |_ ___ _ __   ___  ___ 
 * |  _| | '__/ _` / __| __/ _ \/ __| __/ _ \ '_ \ / _ \/ __|
 * | |___| | | (_| \__ \ || (_) \__ \ ||  __/ | | |  __/\__	\
 * |_____|_|  \__,_|___/\__\___/|___/\__\___|_| |_|\___||___/
 */

#include <stdio.h>
#include "primes.h"


int main()
{
	int num_print;
	scanf(" %d", &num_print);

	int print_indexes[num_print];
	for (int i = 0; i < num_print; i++) {
		scanf(" %d", &print_indexes[i]);
	}

	PrimeList *prime_list = create_prime_list();

	for (int i = 0; i < num_print; i++) {
		printf("%d", get_prime(prime_list, print_indexes[i]));

		if (i < num_print - 1)
			putchar(' ');
	}

	destroy(prime_list);

	return 0;
}
