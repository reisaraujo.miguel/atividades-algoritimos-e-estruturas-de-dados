#include "primes.h"
#include <stdio.h>
#include <stdlib.h>

#define MAX 10000
#define true 1
#define false 0

typedef _Bool bool;

struct prime_list {
	Prime number;
	bool is_prime;
	int next_prime;
};


PrimeList *create_prime_list()
{
	PrimeList *list = malloc(MAX * sizeof(PrimeList));

	for (int i = 1; i <= MAX; i++) {
		list[i - 1].number = i;
		list[i - 1].is_prime = true;
	}

	int end = 0;

	for (int i = 1; i < MAX; i++) {
		if (list[i].is_prime == false) {
			continue;
		}

		list[end].next_prime = i;
		end = i;

		for (int j = end + 1; j < MAX; j++) {
			if (list[j].number % list[end].number == 0) {
				list[j].is_prime = false;
			}
		}
	}

	list[end].next_prime = -1;

	return list;
}


Prime get_prime(PrimeList *list, int index)
{
	int prime = 0;
	for (int i = 0; i < index; i++) {
		prime = list[prime].next_prime;
	}

	return list[prime].number;
}


void destroy(PrimeList *list)
{
	free(list);
}
