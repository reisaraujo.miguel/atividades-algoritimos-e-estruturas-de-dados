#ifndef PRIMES_H
#define PRIMES_H

typedef unsigned int Prime;
typedef struct prime_list PrimeList;

PrimeList *create_prime_list();
/* Cria uma lista de números primos entre 1 e 10000. */

Prime get_prime(PrimeList *list, int index);

void destroy(PrimeList *list);

#endif /* PRIMES_H */
