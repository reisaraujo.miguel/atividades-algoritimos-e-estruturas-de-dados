#ifndef QUEUE_H
#define QUEUE_H

typedef struct person Person;
typedef struct queue Queue;
typedef struct queue_group QueueGroup;

QueueGroup *create();

void insert(QueueGroup *queue_group, Person *person);

int take(QueueGroup *queue_group, Person **person);

int is_empty(Queue *queue);

int is_full(QueueGroup *queue_group);

Person *register_person();

void print_person(Person *person);

void destroy(QueueGroup *queue_group);

char *get_name(Person *person);

int get_age(Person *person);

int have_condition(Person *person);

#endif /* QUEUE_GROUP_H */
