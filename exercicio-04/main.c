/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *  _____ _ _                 _      
 * |  ___(_) | __ _ ___    __| | ___ 
 * | |_  | | |/ _` / __|  / _` |/ _	\
 * |  _| | | | (_| \__ \ | (_| |  __/
 * |_|   |_|_|\__,_|___/  \__,_|\___|
 *                                   
 *  ____       _            _     _           _      
 * |  _ \ _ __(_) ___  _ __(_) __| | __ _  __| | ___ 
 * | |_) | '__| |/ _ \| '__| |/ _` |/ _` |/ _` |/ _	\
 * |  __/| |  | | (_) | |  | | (_| | (_| | (_| |  __/
 * |_|   |_|  |_|\___/|_|  |_|\__,_|\__,_|\__,_|\___|
 */

#include "queue.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define VAZIA -1


int main()
{
	int tot_actions;
	scanf(" %d", &tot_actions);

	QueueGroup *queue_group = create();

	for (int i = 0; i < tot_actions; i++) {
		char action[6];
		scanf(" %s", action);
		getchar();

		Person *person = NULL;

		if (strcmp(action, "ENTRA") == 0) {
			person = register_person();
			insert(queue_group, person);
			free(person);
		} else {
			int _err = take(queue_group, &person);

			if (_err == VAZIA) {
				printf("FILA VAZIA\n");
				free(person);
			} else {
				printf("%s ", get_name(person));
				printf("%d ", get_age(person));
				printf("%d\n", have_condition(person));
			}
		}
	}

	destroy(queue_group);

	return 0;
}
