#include "queue.h"
#include <stdio.h>
#include <stdlib.h>

#define true 1
#define false 0
#define GROUP_TOT 4

typedef unsigned short int uint16_t;
typedef uint16_t bool;

struct person {
	char *name;
	uint16_t age;
	bool have_condition;
};

struct queue {
	Person *queue;
	uint16_t size;
	uint16_t begin;
};

struct queue_group {
	Queue *group;
};


QueueGroup *create()
{
	QueueGroup *temp = malloc(sizeof(QueueGroup));

	temp->group = malloc(GROUP_TOT * sizeof(Queue));

	for (int i = 0; i < GROUP_TOT; i++) {
		temp->group[i].size = 0;
		temp->group[i].begin = 0;
		temp->group[i].queue = NULL;
	}

	return temp;
}


void insert(QueueGroup *queue_group, Person *person)
{
	Queue *group;

	if (person->have_condition && person->age >= 60)
		group = &queue_group->group[0];

	else if (person->have_condition)
		group = &queue_group->group[1];

	else if (person->age >= 60)
		group = &queue_group->group[2];

	else
		group = &queue_group->group[3];

	group->size++;
	group->queue = realloc(group->queue, group->size * sizeof(Person));
	group->queue[group->size - 1] = *person;
}


int take(QueueGroup *queue_group, Person **person)
{
	for (int i = 0; i < GROUP_TOT; i++) {
		if (!is_empty(&queue_group->group[i])) {
			uint16_t begin = queue_group->group[i].begin;

			(*person) = &queue_group->group[i].queue[begin];
			queue_group->group[i].begin++;

			return 0;
		}
	}

	return -1;
}


char *get_name(Person *person)
{
	return person->name;
}


int get_age(Person *person)
{
	return person->age;
}


int have_condition(Person *person)
{
	return person->have_condition;
}


int is_empty(Queue *queue)
{
	if (queue->begin == queue->size)
		return true;
	else
		return false;
}


Person *register_person()
{
	Person *new_person = malloc(sizeof(Person));
	new_person->name = NULL;
	new_person->age = 0;
	new_person->have_condition = false;

	uint16_t index = 0;
	uint16_t size = 0;
	do {
		index = size;
		size++;

		new_person->name = realloc(new_person->name, size * sizeof(char));

		new_person->name[index] = getchar();

	} while (new_person->name[index] != ' ');

	new_person->name[index] = '\0';

	scanf(" %hd", &new_person->age);

	scanf(" %hd", &new_person->have_condition);

	return new_person;
}


void destroy(QueueGroup *queue_group)
{
	for (int i = 0; i < GROUP_TOT; i++) {
		Queue *group = &queue_group->group[i];


		for (int j = 0; j < group->size; j++) {
			free(group->queue[j].name);
		}

		free(group->queue);
	}

	free(queue_group->group);
	free(queue_group);
}
