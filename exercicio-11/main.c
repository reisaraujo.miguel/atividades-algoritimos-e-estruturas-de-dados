/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *  ___                                                     _      
 * |_ _|_ __ ___  _ __  _ __ ___  ___ ___  __ _  ___     __| | ___ 
 *  | || '_ ` _ \| '_ \| '__/ _ \/ __/ __|/ _` |/ _ \   / _` |/ _ \
 *  | || | | | | | |_) | | |  __/\__ \__ \ (_| | (_) | | (_| |  __/
 * |___|_| |_| |_| .__/|_|  \___||___/___/\__,_|\___/   \__,_|\___|
 *               |_|                                               
 *     _                                   
 *    / \   _ ____   _____  _ __ ___  ___  
 *   / _ \ | '__\ \ / / _ \| '__/ _ \/ __| 
 *  / ___ \| |   \ V / (_) | | |  __/\__ \ 
 * /_/   \_\_|    \_/ \___/|_|  \___||___/
 *
 *  ____  _                  _                 _      
 * | __ )(_)_ __   __ _ _ __(_) __ _ ___    __| | ___ 
 * |  _ \| | '_ \ / _` | '__| |/ _` / __|  / _` |/ _ \
 * | |_) | | | | | (_| | |  | | (_| \__	\ | (_| |  __/
 * |____/|_|_| |_|\__,_|_|  |_|\__,_|___/  \__,_|\___|
 *
 *  ____                      
 * | __ ) _   _ ___  ___ __ _ 
 * |  _ \| | | / __|/ __/ _` |
 * | |_) | |_| \__ \ (_| (_| |
 * |____/ \__,_|___/\___\__,_|
 *                                        
 */
#include "binary_search_tree.h"
#include "string_plus.h"
#include <stdio.h>
#include <string.h>


int main()
{
	char *comando = NULL;
	Tree *tree = create_tree();

	while (read_word(&comando, stdin) != EOF) {
		if (strcmp(comando, "insercao") == 0) {
			int value;
			scanf(" %d", &value);

			char aux = getchar();
			if (aux == '\r')
				getchar();

			insert_leaf(tree, value);

		} else if (strcmp(comando, "impressao") == 0) {
			char *ordem = NULL;
			read_word(&ordem, stdin);

			if (get_root(tree) == NULL) {
				printf("VAZIA");

			} else if (strcmp(ordem, "pre-ordem") == 0) {
				pre_ordem(get_root(tree));

			} else if (strcmp(ordem, "em-ordem") == 0) {
				em_ordem(get_root(tree));

			} else if (strcmp(ordem, "pos-ordem") == 0) {
				pos_ordem(get_root(tree));

			} else {
				perror("Ordem inválida!");
			}

			putchar('\n');

			free(ordem);

		} else {
			perror("Comando inválido!");
		}

		free(comando);
	}

	destroy(get_root(tree));

	free(tree);

	return 0;
}
