#include "binary_search_tree.h"
#include <stdio.h>
#include <stdlib.h>

struct leaf {
	int value;
	Leaf *esq;
	Leaf *dir;
};

struct tree {
	Leaf *root;
};


Tree *create_tree()
{
	Tree *new_tree = malloc(sizeof(Tree));
	new_tree->root = NULL;

	return new_tree;
}


void insert_leaf(Tree *tree, int value)
{
	Leaf *new_leaf = malloc(sizeof(Leaf));
	new_leaf->value = value;
	new_leaf->esq = NULL;
	new_leaf->dir = NULL;

	if (tree->root == NULL) {
		tree->root = new_leaf;
		return;
	} else {
		Leaf *root = tree->root;

		while (1) {
			if (value < root->value) {
				if (root->esq != NULL) {
					root = root->esq;
					continue;
				} else {
					root->esq = new_leaf;
					break;
				}
			} else if (value > root->value) {
				if (root->dir != NULL) {
					root = root->dir;
					continue;
				} else {
					root->dir = new_leaf;
					break;
				}
			} else {
				perror("Não é possivel inserir valores repetidos!");
			}
		}
	}
}


Leaf *get_root(Tree *tree)
{
	return tree->root;
}


int pre_ordem(Leaf *root)
{
	if (root == NULL) {
		return 0;
	}

	printf("%d ", root->value);

	pre_ordem(root->esq);
	pre_ordem(root->dir);

	return 0;
}


int em_ordem(Leaf *root)
{
	if (root == NULL) {
		return 0;
	}

	em_ordem(root->esq);

	printf("%d ", root->value);

	em_ordem(root->dir);

	return 0;
}


int pos_ordem(Leaf *root)
{
	if (root == NULL) {
		return 0;
	}

	pos_ordem(root->esq);
	pos_ordem(root->dir);

	printf("%d ", root->value);

	return 0;
}


void destroy(Leaf *root)
{
	if (root->esq == NULL && root->dir == NULL) {
		free(root);
		return;
	}

	if (root->esq != NULL)
		destroy(root->esq);

	if (root->dir != NULL)
		destroy(root->dir);

	free(root);
}
