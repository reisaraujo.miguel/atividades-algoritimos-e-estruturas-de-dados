#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct tree Tree;
typedef struct leaf Leaf;

Tree *create_tree(void);

void insert_leaf(Tree *tree, int value);

Leaf *get_root(Tree *tree);

int pre_ordem(Leaf *root);

int em_ordem(Leaf *root);

int pos_ordem(Leaf *root);

void destroy(Leaf *root);

#endif /* BINARY_SEARCH_TREE_H */
