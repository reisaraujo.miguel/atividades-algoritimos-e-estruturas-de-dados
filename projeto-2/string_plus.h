#ifndef STRING_PLUS_H
#define STRING_PLUS_H

#include <stdio.h>

#define DEFAULT_LINE_SIZE 32
#define DEFAULT_WORD_SIZE 8

/*
 * Lê uma linha inteira (incluindo espaços) até \n ou EOF
 */
int read_line(char **line, FILE *stream);

/*
 * Lê uma palavra até \n, espaço ou EOF
 */
int read_word(char **word, FILE *stream);

/*
 * substitui todas as ocorrências de "typo" por "fix" dentro de "text".
 */
void replace(char *text, char *typo, char *fix);

#endif /* STRING_PLUS_H */
