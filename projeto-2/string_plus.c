#include "string_plus.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define true 1
#define false 0

typedef _Bool bool;


void replace(char *text, char *typo, char *fix)
{
	int text_size = strlen(text) < 101 ? strlen(text) : 101;
	int typo_size = strlen(typo);
	int fix_size = strlen(fix);
	char new_text[101];
	int index = 0;

	for (int i = 0; i < text_size; i++) {
		if (strncmp(&text[i], typo, typo_size) == 0) {
			for (int j = 0; j < fix_size; j++) {
				new_text[index] = fix[j];
				index++;
			}

			i += typo_size - 1;

			continue;
		}

		new_text[index] = text[i];
		index++;
	}

	for (int i = 0; i < index; i++) {
		text[i] = new_text[i];
	}

	if (text[index] != '\0')
		text[index] = '\0';
}


int read_line(char **line, FILE *stream)
{
	//consumindo caracteres inválidos no buffer
	bool stop = false;
	char temp;

	do {
		stop = fscanf(stream, "%c", &temp) == EOF;
	} while (!stop && (temp == '\n' || temp == '\r' || temp == ' '));

	if (stop) {
		return EOF;
	}

	ungetc(temp, stream);

	int size = DEFAULT_LINE_SIZE;
	char *linha_lida = malloc(size * sizeof(char));
	int indice = -1;

	do {
		indice++;

		if (indice == size) {
			size += DEFAULT_LINE_SIZE;
			linha_lida = realloc(linha_lida, size * sizeof(char));
		}

		stop = fscanf(stream, "%c", &linha_lida[indice]) == EOF;

	} while (!stop && linha_lida[indice] != '\n' && linha_lida[indice] != '\r');

	if (!stop && linha_lida[indice] == '\r') {
		getc(stream);
	}

	linha_lida[indice] = '\0';

	linha_lida = realloc(linha_lida, strlen(linha_lida));

	(*line) = linha_lida;

	return 0;
}


int read_word(char **word, FILE *stream)
{
	//consumindo caracteres inválidos no buffer
	bool stop = false;
	char temp;

	do {
		stop = fscanf(stream, "%c", &temp) == EOF;
	} while (!stop && (temp == '\n' || temp == '\r' || temp == ' '));

	if (stop) {
		return EOF;
	}

	ungetc(temp, stream);

	int size = DEFAULT_WORD_SIZE;
	char *palavra_lida = malloc(size * sizeof(char));
	int indice = -1;

	do {
		indice++;

		if (indice == size) {
			size += DEFAULT_WORD_SIZE;
			palavra_lida = realloc(palavra_lida, size * sizeof(char));
		}

		stop = fscanf(stream, "%c", &palavra_lida[indice]) == EOF;

	} while (!stop && palavra_lida[indice] != '\n' &&
			 palavra_lida[indice] != '\r' && palavra_lida[indice] != ' ');

	if (!stop && palavra_lida[indice] == '\r') {
		getc(stream);
	}

	palavra_lida[indice] = '\0';

	palavra_lida = realloc(palavra_lida, strlen(palavra_lida));

	*word = palavra_lida;

	return 0;
}
