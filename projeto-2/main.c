/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *     _                                    _____                         
 *    / \   _ ____   _____  _ __ ___  ___  |_   _| __ ___  __ _ _ __  ___ 
 *   / _ \ | '__\ \ / / _ \| '__/ _ \/ __|   | || '__/ _ \/ _` | '_ \/ __|
 *  / ___ \| |   \ V / (_) | | |  __/\__ \   | || | |  __/ (_| | |_) \__ \
 * /_/   \_\_|    \_/ \___/|_|  \___||___/   |_||_|  \___|\__,_| .__/|___/
 *                                                             |_|        
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "treaps.h"
#include "string_plus.h"

void imprimir(Node *raiz, char *modo);


int main()
{
	int qt_comandos = 0;
	scanf(" %d", &qt_comandos);

	Treap *treap = create_treap();

	for (int i = 0; i < qt_comandos; i++) {
		char *comando = NULL;
		read_word(&comando, stdin);

		if (strcmp(comando, "insercao") == 0) {
			int chave, prioridade;
			scanf(" %d %d", &chave, &prioridade);

			insert_node(treap, prioridade, chave);

		} else if (strcmp(comando, "remocao") == 0) {
			int chave;
			scanf(" %d", &chave);

			delete_node(treap, chave);

		} else if (strcmp(comando, "impressao") == 0) {
			char *modo = NULL;
			read_word(&modo, stdin);

			imprimir(get_root(treap), modo);

			free(modo);

		} else if (strcmp(comando, "buscar") == 0) {
			int chave;
			scanf(" %d", &chave);

			printf("%d\n", search_node(get_root(treap), chave));

		} else {
			printf("Comando inválido: %s\n", comando);
		}

		free(comando);
	}

	destroy(get_root(treap));

	free(treap);

	return 0;
}


void imprimir(Node *raiz, char *modo)
{
	if (strcmp(modo, "preordem") == 0) {
		impressao_preordem(raiz);

	} else if (strcmp(modo, "ordem") == 0) {
		impressao_em_ordem(raiz);

	} else if (strcmp(modo, "posordem") == 0) {
		impressao_posordem(raiz);

	} else if (strcmp(modo, "largura") == 0) {
		impressao_largura(raiz);

	} else {
		printf("Modo inválido");
	}

	putchar('\n');
}
