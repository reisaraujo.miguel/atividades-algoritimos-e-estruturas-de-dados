#ifndef TREAPS_H
#define TREAPS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct treap Treap;
typedef struct node Node;

/* retorna uma treap com raiz == NULL */
Treap *create_treap(void);

void insert_node(Treap *treap, int prioridade, int chave);

Node *get_root(Treap *treap);

/* rotaciona para esquerda e retorna nova raiz */
Node *rotacao_ee(Node *desbalanceado);

/* rotaciona para direita e retorna nova raiz */
Node *rotacao_dd(Node *desbalanceado);

void delete_node(Treap *treap, int chave);

/* retorna 1 se nó existir, 0 caso contrário */
int search_node(Node *raiz, int chave);

void impressao_preordem(Node *raiz);

void impressao_em_ordem(Node *raiz);

void impressao_posordem(Node *raiz);

void impressao_largura(Node *raiz);

void destroy(Node *raiz);

#endif /* TREAPS_H */
