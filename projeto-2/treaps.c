#include "treaps.h"
#include <stdio.h>
#include <stdlib.h>

struct treap {
	Node *raiz;
};

struct node {
	int chave;
	int prioridade;
	Node *esq;
	Node *dir;
};

Node *insert_recursive(Node *raiz, Node *new_node);
Node *delete_recursive(Node *raiz, int chave);


Treap *create_treap()
{
	Treap *new_treap = malloc(sizeof(Treap));
	new_treap->raiz = NULL;

	return new_treap;
}


Node *get_root(Treap *treap)
{
	return treap->raiz;
}


void insert_node(Treap *treap, int prioridade, int chave)
{
	Node *new_node = malloc(sizeof(Node));
	new_node->prioridade = prioridade;
	new_node->chave = chave;
	new_node->dir = NULL;
	new_node->esq = NULL;

	if (treap->raiz == NULL) {
		treap->raiz = new_node;
	} else {
		treap->raiz = insert_recursive(treap->raiz, new_node);
	}
}


Node *insert_recursive(Node *raiz, Node *new_node)
{
	if (raiz == NULL) {
		return new_node;

	} else if (raiz->chave > new_node->chave) {
		raiz->esq = insert_recursive(raiz->esq, new_node);

		if (raiz->esq->prioridade > raiz->prioridade)
			return rotacao_dd(raiz);

	} else if (raiz->chave < new_node->chave) {
		raiz->dir = insert_recursive(raiz->dir, new_node);

		if (raiz->dir->prioridade > raiz->prioridade)
			return rotacao_ee(raiz);

	} else {
		printf("Elemento ja existente\n");
		free(new_node);
	}

	return raiz;
}


Node *rotacao_ee(Node *desbalanceado)
{
	Node *aux;
	aux = desbalanceado->dir;
	desbalanceado->dir = aux->esq;
	aux->esq = desbalanceado;

	return aux;
}


Node *rotacao_dd(Node *desbalanceado)
{
	Node *aux;
	aux = desbalanceado->esq;
	desbalanceado->esq = aux->dir;
	aux->dir = desbalanceado;

	return aux;
}


void delete_node(Treap *treap, int chave)
{
	treap->raiz = delete_recursive(treap->raiz, chave);
}


Node *delete_recursive(Node *raiz, int chave)
{
	if (raiz == NULL) {
		printf("Chave nao localizada\n");

	} else if (raiz->chave > chave) {
		raiz->esq = delete_recursive(raiz->esq, chave);

	} else if (raiz->chave < chave) {
		raiz->dir = delete_recursive(raiz->dir, chave);

	} else if (raiz->chave == chave) {
		if (raiz->dir == NULL && raiz->esq == NULL) {
			free(raiz);

			return NULL;

		} else if (raiz->dir == NULL) {
			Node *nova_raiz = raiz->esq;
			free(raiz);

			return nova_raiz;

		} else if (raiz->esq == NULL) {
			Node *nova_raiz = raiz->dir;
			free(raiz);

			return nova_raiz;

		} else {
			Node *nova_raiz = rotacao_ee(raiz);
			nova_raiz->esq = delete_recursive(nova_raiz->esq, chave);

			return nova_raiz;
		}
	}

	return raiz;
}


int search_node(Node *raiz, int chave)
{
	if (raiz == NULL) {
		return 0;

	} else if (raiz->chave == chave) {
		return 1;

	} else if (raiz->chave > chave) {
		return search_node(raiz->esq, chave);

	} else if (raiz->chave < chave) {
		return search_node(raiz->dir, chave);
	}

	return 0;
}


void impressao_preordem(Node *raiz)
{
	if (raiz == NULL) {
		return;
	}

	printf("(%d, %d) ", raiz->chave, raiz->prioridade);

	impressao_preordem(raiz->esq);
	impressao_preordem(raiz->dir);
}


void impressao_em_ordem(Node *raiz)
{
	if (raiz == NULL) {
		return;
	}

	impressao_em_ordem(raiz->esq);

	printf("(%d, %d) ", raiz->chave, raiz->prioridade);

	impressao_em_ordem(raiz->dir);
}


void impressao_posordem(Node *raiz)
{
	if (raiz == NULL) {
		return;
	}

	impressao_posordem(raiz->esq);
	impressao_posordem(raiz->dir);

	printf("(%d, %d) ", raiz->chave, raiz->prioridade);
}


void impressao_largura(Node *raiz)
{
	Node **fila = malloc(sizeof(Node *));
	int indice = 0;
	int tamanho_fila = 1;

	fila[tamanho_fila - 1] = raiz;

	while (indice < tamanho_fila) {
		if (fila[indice]->esq != NULL) {
			tamanho_fila++;

			fila = realloc(fila, tamanho_fila * sizeof(Node *));

			fila[tamanho_fila - 1] = fila[indice]->esq;
		}

		if (fila[indice]->dir != NULL) {
			tamanho_fila++;

			fila = realloc(fila, tamanho_fila * sizeof(Node *));

			fila[tamanho_fila - 1] = fila[indice]->dir;
		}

		indice++;
	}

	for (int i = 0; i < tamanho_fila; i++) {
		printf("(%d, %d) ", fila[i]->chave, fila[i]->prioridade);
	}

	free(fila);
}


void destroy(Node *raiz)
{
	if (raiz->esq == NULL && raiz->dir == NULL) {
		free(raiz);
		return;
	}

	if (raiz->esq != NULL)
		destroy(raiz->esq);

	if (raiz->dir != NULL)
		destroy(raiz->dir);

	free(raiz);
}
