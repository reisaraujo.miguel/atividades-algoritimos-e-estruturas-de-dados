#ifndef BIGNUM_H
#define BIGNUM_H

#include <stdbool.h>

typedef struct big Big;

Big *read_big(void);

int insert(Big *big_number_head, Big *number);

//recebe uma lista de big numbers e retorna a soma de todos os números
Big *sum(Big *big_number_1, Big *big_number_2);

bool bigger(Big *big_number_1, Big *big_number_2);

bool smaller(Big *big_number_1, Big *big_number_2);

bool equal(Big *big_number_1, Big *big_number_2);

void print_big(Big *big_number);

int get_number(Big *number);

void destroy(Big *big_number);

#endif /* BIGNUM_H */
