#include "bignum.h"
#include "string_plus.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LIMIT_NUMBER 10000
#define MAX_DIGIT 4

struct big {
	union {
		int number;
		int tot_nodes;
	};
	Big *last;
	Big *next;
};


Big *read_big(void)
{
	Big *head = malloc(sizeof(Big));
	head->last = NULL;
	head->next = NULL;
	head->tot_nodes = 0;

	char *num = read_word(stdin);
	int size = strlen(num);

	int i = 0;
	//remove zeros à esquerda
	while (i < size) {
		if (num[i] == '0') {
			i++;
		} else {
			size -= i;
			char *num_temp = malloc(size * sizeof(char));
			strcpy(num_temp, &num[i]);

			free(num);

			num = num_temp;
			break;
		}
	}

	while (size >= MAX_DIGIT) {
		Big *new_number = malloc(sizeof(Big));

		new_number->next = NULL;
		new_number->last = NULL;

		char *number = malloc((MAX_DIGIT + 1) * sizeof(char));

		size -= MAX_DIGIT;

		strncpy(number, &num[size], MAX_DIGIT);

		number[MAX_DIGIT] = '\0';

		new_number->number = atoi(number);

		if (size == 1 && num[0] == '-') {
			new_number->number *= -1;
			size--;
		}

		insert(head, new_number);

		free(number);
	}


	if (size > 0) {
		Big *new_number = malloc(sizeof(Big));

		new_number->next = NULL;
		new_number->last = NULL;

		char *number = malloc((size + 1) * sizeof(char));

		strncpy(number, &num[0], size);

		number[size] = '\0';

		new_number->number = atoi(number);

		insert(head, new_number);

		free(number);
	}

	free(num);

	return head;
}


int insert(Big *big_number_head, Big *number)
{
	if (big_number_head->next == NULL && big_number_head->last == NULL) {
		big_number_head->next = number;
		big_number_head->last = number;

	} else {
		number->last = big_number_head->last;
		big_number_head->last->next = number;
		big_number_head->last = number;
	}

	big_number_head->tot_nodes++;

	return 0;
}


Big *sum(Big *big_number_1, Big *big_number_2)
{
	Big *new_big_number = malloc(sizeof(Big));
	new_big_number->last = NULL;
	new_big_number->next = NULL;
	new_big_number->tot_nodes = 0;

	bool are_both_negative =
		(big_number_1->last->number < 0 && big_number_2->last->number < 0);

	if (are_both_negative) {
		big_number_1->last->number *= -1;
		big_number_2->last->number *= -1;
	}

	int vaium = 0;

	while (big_number_1->next != NULL && big_number_2->next != NULL) {
		Big *new_number = malloc(sizeof(Big));
		new_number->next = NULL;
		new_number->last = NULL;

		int number_1 = big_number_1->next->number;
		int number_2 = big_number_2->next->number;

		int number_3 = number_1 + number_2 + vaium;

		vaium = number_3 / LIMIT_NUMBER;

		if (vaium > 0) {
			new_number->number = number_3 - LIMIT_NUMBER;
		} else {
			new_number->number = number_3;
		}

		insert(new_big_number, new_number);

		big_number_1 = big_number_1->next;
		big_number_2 = big_number_2->next;
	}

	if (big_number_1->next != NULL) {
		while (big_number_1->next != NULL) {
			Big *new_number = malloc(sizeof(Big));
			new_number->next = NULL;
			new_number->last = NULL;

			int number_1 = big_number_1->next->number;

			int number_3 = number_1 + vaium;

			vaium = number_3 / LIMIT_NUMBER;

			if (vaium > 0) {
				new_number->number = number_3 - 10000;
			} else {
				new_number->number = number_3;
			}

			insert(new_big_number, new_number);

			big_number_1 = big_number_1->next;
		}
	}

	if (big_number_2->next != NULL) {
		while (big_number_2->next != NULL) {
			Big *new_number = malloc(sizeof(Big));
			new_number->next = NULL;
			new_number->last = NULL;

			int number_2 = big_number_2->next->number;

			int number_3 = number_2 + vaium;

			vaium = number_3 / LIMIT_NUMBER;

			if (vaium > 0) {
				new_number->number = number_3 - LIMIT_NUMBER;
			} else {
				new_number->number = number_3;
			}

			insert(new_big_number, new_number);

			big_number_2 = big_number_2->next;
		}
	}

	if (vaium > 0) {
		Big *new_number = malloc(sizeof(Big));
		new_number->next = NULL;
		new_number->last = NULL;

		new_number->number = vaium;

		insert(new_big_number, new_number);
	}

	if (are_both_negative) {
		new_big_number->last->number *= -1;
	}

	//conserta problema onde operações como
	//"SUM -10000 10000" resultam em "00000"
	//mas parece que esse não era o problema AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	if (new_big_number->last->number == 0 &&
		new_big_number->last->last != NULL) {
		Big *temp1 = new_big_number;
		Big *temp2 = new_big_number;

		temp1 = temp1->last->last;
		temp2 = temp2->last->last->last;

		while (temp1 != NULL && temp1->number == 0 && temp1 != new_big_number) {
			free(temp1);

			new_big_number->last->last = temp2;

			if (temp2 == new_big_number || temp2 == NULL)
				break;

			temp1 = temp2;
			temp2 = temp2->last;
		}
	}

	return new_big_number;
}


bool bigger(Big *big_number_1, Big *big_number_2)
{
	if (big_number_1->last->number < 0 && big_number_2->last->number >= 0) {
		return false;
	} else if (big_number_2->last->number < 0 &&
			   big_number_1->last->number >= 0) {
		return true;
	} else if (big_number_1->tot_nodes > big_number_2->tot_nodes) {
		return true;
	} else if (big_number_2->tot_nodes > big_number_1->tot_nodes) {
		return false;
	} else if (big_number_1->last->number > big_number_2->last->number) {
		return true;
	} else {
		return false;
	}
}

bool smaller(Big *big_number_1, Big *big_number_2)
{
	if (big_number_1->last->number < 0 && big_number_2->last->number >= 0) {
		return true;
	} else if (big_number_2->last->number < 0 &&
			   big_number_1->last->number >= 0) {
		return false;
	} else if (big_number_1->tot_nodes < big_number_2->tot_nodes) {
		return true;
	} else if (big_number_2->tot_nodes < big_number_1->tot_nodes) {
		return false;
	} else if (big_number_1->last->number < big_number_2->last->number) {
		return true;
	} else {
		return false;
	}
}

bool equal(Big *big_number_1, Big *big_number_2)
{
	if (big_number_1->tot_nodes != big_number_2->tot_nodes) {
		return false;
	} else {
		while (big_number_1->last != NULL) {
			if (big_number_1->last->number != big_number_2->last->number) {
				return false;
			} else {
				big_number_1 = big_number_1->last;
				big_number_2 = big_number_2->last;
			}
		}
	}

	return true;
}


void print_big(Big *big_number)
{
	printf("%d", big_number->last->number);

	big_number = big_number->last;

	while (big_number->last != NULL) {
		printf("%04d", big_number->last->number);
		big_number = big_number->last;
	}
}


int get_number(Big *number)
{
	return number->number;
}


void destroy(Big *big_number)
{
	Big *last = big_number->last;

	free(big_number);

	if (last != NULL)
		destroy(last);
}
