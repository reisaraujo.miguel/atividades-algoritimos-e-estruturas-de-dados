#ifndef STRING_PLUS_H
#define STRING_PLUS_H

#include <stdio.h>

/*
 * Lê uma linha inteira (incluindo espaços) até \n ou EOF
 */
char *read_line(FILE *stream);

/*
 * Lê uma palavra até \n, espaço ou EOF
 */
char *read_word(FILE *stream);

/*
 * substitui todas as ocorrências de "typo" por "fix" dentro de "text".
 */
void replace(char *text, char *typo, char *fix);

#endif /* STRING_PLUS_H */
