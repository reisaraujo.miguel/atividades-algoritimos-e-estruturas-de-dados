/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0202
 *
 * Título Trabalho:
 *  ____       _                             __  __       _     
 * |  _ \ __ _| | __ ___   ___ __ __ _ ___  |  \/  | __ _(_)___ 
 * | |_) / _` | |/ _` \ \ / / '__/ _` / __| | |\/| |/ _` | / __|
 * |  __/ (_| | | (_| |\ V /| | | (_| \__ \ | |  | | (_| | \__	\
 * |_|   \__,_|_|\__,_| \_/ |_|  \__,_|___/ |_|  |_|\__,_|_|___/
 *                                                            
 *  _____                                _            
 * |  ___| __ ___  __ _ _   _  ___ _ __ | |_ ___  ___ 
 * | |_ | '__/ _ \/ _` | | | |/ _ \ '_ \| __/ _ \/ __|
 * |  _|| | |  __/ (_| | |_| |  __/ | | | ||  __/\__ \
 * |_|  |_|  \___|\__, |\__,_|\___|_| |_|\__\___||___/
 *                   |_|                              
 */

#include "list.h"
#include <stdio.h>
#include <string.h>

void print_repeating_words();

int main()
{
	_Bool first_run = 1;
	int last_char = 0;

	while (1) {
		last_char = getchar();
		if (last_char == EOF)
			break;

		ungetc(last_char, stdin);

		if (!first_run)
			putchar('\n');

		first_run = 0;
		
		print_repeating_words();
	}

	return 0;
}


void print_repeating_words()
{
	char text[400];
		int i = 0;
		do {
			text[i] = getchar();
			i++;
		} while (text[i - 1] != '\n' && text[i - 1] != EOF &&
				 text[i - 1] != '\r');

		if (text[i - 1] == '\r')
			getchar();

		text[i - 1] = '\0';

		int list_size;
		RepeatingWord *list = create_repeating_word_list(text, &list_size);

		sort_repeating_word_list(list, 0, list_size - 1);

		int num_lines;
		scanf(" %d", &num_lines);
		getchar();
		getchar();

		if (num_lines > list_size) {
			num_lines = list_size;
		}

		for (int i = 0; i < num_lines; i++) {
			printf("%s %d\n", get_word(list, i), get_count(list, i));
		}

		destroy(list, list_size);
}
