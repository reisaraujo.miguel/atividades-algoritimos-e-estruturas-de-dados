#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define true 1
#define false 0

typedef _Bool bool;

struct repeating_word {
	char *word;
	int count;
};

char *read_word(char *text);
void merge(RepeatingWord *list, int begin, int middle, int end);


void sort_repeating_word_list(RepeatingWord *list, int begin, int end)
{
	if (begin >= end)
		return;

	int middle = (begin + end) / 2;

	sort_repeating_word_list(list, begin, middle);
	sort_repeating_word_list(list, middle + 1, end);

	merge(list, begin, middle, end);
}


void merge(RepeatingWord *list, int begin, int middle, int end)
{
	RepeatingWord temp[(end - begin) + 1];

	int index_first_half = begin;
	int index_second_half = middle + 1;
	int index_temp = 0;

	while (index_first_half <= middle && index_second_half <= end) {
		if (list[index_first_half].count > list[index_second_half].count) {
			temp[index_temp] = list[index_first_half];
			index_first_half++;

		} else if (list[index_first_half].count <
				   list[index_second_half].count) {
			temp[index_temp] = list[index_second_half];
			index_second_half++;

		} else if (strcmp(list[index_first_half].word,
						  list[index_second_half].word) < 0) {
			temp[index_temp] = list[index_first_half];
			index_first_half++;

		} else {
			temp[index_temp] = list[index_second_half];
			index_second_half++;
		}

		index_temp++;
	}

	while (index_first_half <= middle) {
		temp[index_temp] = list[index_first_half];
		index_first_half++;
		index_temp++;
	}

	while (index_second_half <= end) {
		temp[index_temp] = list[index_second_half];
		index_second_half++;
		index_temp++;
	}

	index_temp = 0;

	for (int i = begin; i <= end; i++) {
		list[i] = temp[index_temp];
		index_temp++;
	}
}


RepeatingWord *create_repeating_word_list(char *text, int *list_size)
{
	int index = 0;
	int text_size = strlen(text);
	RepeatingWord *list = NULL;
	(*list_size) = 0;
	int list_index = 0;

	while (index < text_size) {
		char *word = read_word(&text[index]);
		index += strlen(word) + 1;

		bool skip = false;
		for (int i = 0; i < (*list_size); i++) {
			if (strcmp(list[i].word, word) == 0) {
				list[i].count++;
				skip = true;
				break;
			}
		}

		if (skip)
			continue;

		list_index = (*list_size);
		(*list_size)++;

		list = realloc(list, (*list_size) * sizeof(RepeatingWord));

		list[list_index].word = word;
		list[list_index].count = 1;
	}

	return list;
}


char *get_word(RepeatingWord *list, int index)
{
	return list[index].word;
}


int get_count(RepeatingWord *list, int index)
{
	return list[index].count;
}


void destroy(RepeatingWord *list, int list_size)
{
	for (int i = 0; i < list_size; i++) {
		free(list[i].word);
	}

	free(list);
}


char *read_word(char *text)
{
	int characters_counter = 0;
	int index = 0;
	char *word = NULL;

	do {
		index = characters_counter;

		characters_counter += 1;

		word = realloc(word, characters_counter * sizeof(char));

		word[index] = text[index];

	} while (word[index] != '\0' && word[index] != ' ');

	word[index] = '\0';

	return word;
}
