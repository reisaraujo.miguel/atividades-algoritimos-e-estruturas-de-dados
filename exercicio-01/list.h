#pragma once

typedef struct repeating_word RepeatingWord;

void sort_repeating_word_list(RepeatingWord *list, int begin, int end);
/*
 * Ordena as repeating_words dentro de "list", a partir de "begin" até "end",
 * por ordem decrescente da quantidade de vezes que cada repeating_word se
 * repete.
 */


RepeatingWord *create_repeating_word_list(char *text, int *list_size);
/*
 * Cria uma lista com repeating_words únicas coletadas de "stdin", e a
 * respectiva quantidade de vezes que essas repeating_words se repetem.
 *
 * Retorna o tamanho da lista em "list_size".
 */

char *get_word(RepeatingWord *list, int index);

int get_count(RepeatingWord *list, int index);

void destroy(RepeatingWord *list, int list_size);
