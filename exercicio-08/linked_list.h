#ifndef LINKED_LIST_H
#define LINKED_LIST_H

typedef struct polinomio Polinomio;

Polinomio *create_polinomio(void);

int insert(Polinomio *list, Polinomio *person);

Polinomio *sum(Polinomio **list, int tot);

void print_polinomio(Polinomio *polinomio);

int get_coeff(Polinomio *person);

Polinomio *register_coeff(void);

void destroy(Polinomio *list);

#endif /* LINKED_LIST_H */
