#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>

struct polinomio {
	union {
		int coeff;
		Polinomio *last;
	};

	Polinomio *next;
};


Polinomio *create_polinomio(void)
{
	Polinomio *head = malloc(sizeof(Polinomio));
	head->last = NULL;
	head->next = NULL;

	return head;
}


int insert(Polinomio *list, Polinomio *coeff)
{
	if (list->next == NULL && list->last == NULL) { // lista vazia
		list->next = coeff;
		list->last = coeff;

	} else {
		list->last->next = coeff;
		list->last = coeff;
	}

	return 0;
}


Polinomio *sum(Polinomio **list, int tot)
{
	Polinomio *new_polinomio = create_polinomio();

	int counter = 0;

	while (counter < tot) {
		Polinomio *polinimio_coeff = list[counter];
		Polinomio *new_polinomio_coeff = new_polinomio;

		while (polinimio_coeff->next != NULL) {
			if (new_polinomio_coeff->next == NULL) {
				Polinomio *new_coeff = malloc(sizeof(Polinomio));

				new_coeff->coeff = get_coeff(polinimio_coeff->next);

				new_coeff->next = NULL;

				new_polinomio_coeff->next = new_coeff;

			} else {
				new_polinomio_coeff->next->coeff +=
					get_coeff(polinimio_coeff->next);
			}

			new_polinomio_coeff = new_polinomio_coeff->next;
			polinimio_coeff = polinimio_coeff->next;
		}

		counter++;
	}

	return new_polinomio;
}


void print_polinomio(Polinomio *polinomio)
{
	if (polinomio->next == NULL && polinomio->last == NULL) {
		printf("VAZIA\n");
		return;
	}

	printf("(%d", polinomio->next->coeff);

	polinomio = polinomio->next;

	while (polinomio->next != NULL) {
		printf(",%d", polinomio->next->coeff);
		polinomio = polinomio->next;
	}

	putchar(')');
}


int get_coeff(Polinomio *coeff)
{
	return coeff->coeff;
}


Polinomio *register_coeff()
{
	Polinomio *new_coeff = malloc(sizeof(Polinomio));

	scanf(" %d", &new_coeff->coeff);
	//getchar();

	new_coeff->next = NULL;

	return new_coeff;
}


void destroy(Polinomio *list)
{
	Polinomio *next = list->next;

	free(list);

	if (next != NULL)
		destroy(next);
}
