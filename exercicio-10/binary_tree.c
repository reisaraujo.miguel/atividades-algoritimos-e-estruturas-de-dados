#include "binary_tree.h"
#include <stdlib.h>

struct leaf {
	int id;
	int value;
	int id_esq;
	int id_dir;
	Leaf *esq;
	Leaf *dir;
};

struct tree {
	Leaf *root;
};


Tree *create_tree()
{
	Tree *new_tree = malloc(sizeof(Tree));
	new_tree->root = NULL;

	return new_tree;
}


void insert_leaf(Tree *tree, int id, int value, int id_esq, int id_dir)
{
	Leaf *new_leaf = malloc(sizeof(Leaf));
	new_leaf->id = id;
	new_leaf->value = value;
	new_leaf->id_esq = id_esq;
	new_leaf->id_dir = id_dir;
	new_leaf->esq = NULL;
	new_leaf->dir = NULL;

	if (tree->root == NULL) {
		tree->root = new_leaf;
		return;
	}

	Leaf *parent = search_parent(tree->root, id);

	if (parent->id_dir == id) {
		parent->dir = new_leaf;
	} else {
		parent->esq = new_leaf;
	}
}


Leaf *search_parent(Leaf *root, int id)
{
	if (root->id_dir == id || root->id_esq == id) {
		return root;
	}

	if (root->esq != NULL) {
		Leaf *esq = search_parent(root->esq, id);

		if (esq != NULL) {
			return esq;
		}
	}

	if (root->dir != NULL) {
		Leaf *dir = search_parent(root->dir, id);

		if (dir != NULL) {
			return dir;
		}
	}

	return NULL;
}


Leaf *get_root(Tree *tree)
{
	return tree->root;
}


int eql_sum(Leaf *root)
{
	if (root->id_esq == -1 && root->id_dir == -1) {
		return root->value;
	}

	int value_esq = 0;
	int value_dir = 0;

	if (root->esq != NULL) {
		value_esq = eql_sum(root->esq);
		if (value_esq == -1)
			return -1;
	}

	if (root->dir != NULL) {
		value_dir = eql_sum(root->dir);
		if (value_dir == -1)
			return -1;
	}

	if ((value_dir + value_esq) == root->value) {
		return root->value;
	} else {
		return -1;
	}
}


void destroy(Leaf *root)
{
	if (root->id_esq == -1 && root->id_dir == -1) {
		free(root);
		return;
	}

	if (root->esq != NULL)
		destroy(root->esq);

	if (root->dir != NULL)
		destroy(root->dir);

	free(root);
}
