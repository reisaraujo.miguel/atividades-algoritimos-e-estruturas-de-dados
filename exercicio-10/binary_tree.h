#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct tree Tree;
typedef struct leaf Leaf;

Tree *create_tree(void);

void insert_leaf(Tree *tree, int id, int value, int id_esq, int id_dir);

Leaf *search_parent(Leaf *root, int id);

Leaf *get_root(Tree *tree);

int eql_sum(Leaf *root);

void destroy(Leaf *root);

#endif /* BINARY_TREE_H */
